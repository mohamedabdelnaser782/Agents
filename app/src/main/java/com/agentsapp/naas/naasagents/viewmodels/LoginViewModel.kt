package com.agentsapp.naas.naasagents.viewmodels


import android.app.Activity
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Intent
import com.agentsapp.naas.naasagents.R
import com.agentsapp.naas.naasagents.data.models.Language
import com.agentsapp.naas.naasagents.data.models.User
import com.agentsapp.naas.naasagents.ui.activity.Home_Screen
import com.agentsapp.naas.naasagents.ui.fragment.FragmentLogin
import com.agentsapp.naas.naasagents.utl.LanguageUtilty
import com.agentsapp.naas.naasagents.utl.ViewUtl

class LoginViewModel(activity: Activity)  : ViewModel( ) {
    private val user: User
    val activity=activity
    var mCurrentName = MutableLiveData<User> ()

    val userLiveData:LiveData<User>
    get() =mCurrentName

    init {
        user = User()
    }


    fun afterUserNameTextChanged(s: CharSequence) {
        user.mUsername=s.toString()
    }

    fun afterPasswordTextChanged(s: CharSequence) {
        user.mPassword=s.toString()
    }

    fun onLoginClicked() {
        println("username is "+user.mUsername)
        if (user.isInputDataValid()) {
            user.login()
            mCurrentName.postValue(user)
            goToHome()
        } else {
            println("errorMessage")
        }
    }

    fun goToHome(){
        val intent = Intent(activity, Home_Screen::class.java)
        activity.overridePendingTransition(0,0)
        activity.startActivity(intent)
        activity.overridePendingTransition(0,0)
    }

  fun  onClickChangeLanguage(language:String){
      if (LanguageUtilty(activity).isChangeLanguage(language)){
          val intent=Intent(activity,activity::class.java)
          intent.putExtra("defualt",true)
          ViewUtl().refresh(activity,intent);
          Language(activity).lang=language
      }
  }
}
