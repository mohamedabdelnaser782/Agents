package com.agentsapp.naas.naasagents.data.local;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.widget.Toast;

import com.agentsapp.naas.naasagents.viewmodels.ApplicationClass;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class SaveShared {

    Boolean c = true;
    Handler handler =new Handler();
    SharedPreferences preferences;
    Context activity;

    public SaveShared( String key) {
        this.activity = ApplicationClass.getMyApplicationContext();
        preferences=activity.getSharedPreferences(key, MODE_PRIVATE);
    }

   public void saveObject(HashMap<String,String>sharedData){
            for (Map.Entry<String, String> entry : sharedData.entrySet()) {
                final String key = entry.getKey();
                final String value = entry.getValue();
                preferences.edit().putString(key,value).apply();
                handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, key+" : "+preferences.getString(key,""), Toast.LENGTH_SHORT).show();
            }
        });
     }
    }

   public void saveElement(final String key, String value){
        preferences.edit().putString(key,value).apply();
    }

    public void clear(){

        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();

    }

    public Boolean isLoged(){

        if (preferences.getAll().size()==0) {
            c = false;
        }
        return c;
    }


   public String getPreferences(String key){
      return preferences.getString(key,"");
    }

}
