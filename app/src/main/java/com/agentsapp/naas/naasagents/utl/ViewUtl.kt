package com.agentsapp.naas.naasagents.utl

import android.app.Activity
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import com.agentsapp.naas.naasagents.R
import com.agentsapp.naas.naasagents.ui.activity.SplashScreen

class ViewUtl {

    fun move(container: Int, fragment: Fragment, tag: String, manager: FragmentManager,isTransaction:Boolean =true) {
        val fragmentTransaction: FragmentTransaction= manager.beginTransaction()
        if (isTransaction) {
            fragmentTransaction.setCustomAnimations(R.anim.abc_slide_in_top, R.anim.abc_slide_in_bottom)
        }
        fragmentTransaction.replace(container, fragment, tag)
        fragmentTransaction.addToBackStack(null).commit()

    }

    fun refresh(requireActivity:Activity,intent: Intent){
        requireActivity.overridePendingTransition(0,0)
        requireActivity.startActivity(intent)
        requireActivity.overridePendingTransition(0,0)

    }

    fun currentFragment(fragmentManager: FragmentManager,tag: String): Fragment {
        return fragmentManager.findFragmentByTag(tag)
    }
}