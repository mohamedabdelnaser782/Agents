package com.agentsapp.naas.naasagents.data.models

import android.arch.lifecycle.MutableLiveData
import android.support.annotation.NonNull
import com.agentsapp.naas.naasagents.data.local.SaveShared
import com.agentsapp.naas.naasagents.data.webservice.*
import com.google.gson.annotations.SerializedName
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class User (@NonNull @field:SerializedName("userName") var userName: String="", @NonNull @field:SerializedName("password") var password: String=""){

    private var mCompositeDisposable: CompositeDisposable? = null
    val saveShared=SaveShared(User::class.java.name)



    @SerializedName("userName")
     var mUsername: String = userName
        set(value) {
             field = value
             saveShared.saveElement("username",field)
             println("username has been saved"+field)

         }

     @SerializedName("Password")
     var mPassword: String = password
         set(value) {
             field = value
             saveShared.saveElement("password",field)
             println("password has been saved")
         }

     @SerializedName("access_token")
     var accessToken:String=""

    companion object

    fun isInputDataValid(): Boolean {
        return mUsername.length>4 && mPassword.length > 4
    }



     fun login() {
        mCompositeDisposable = CompositeDisposable()
        println("loadin...json")
        val requestInterface = ApiClient().getInstance()
        mCompositeDisposable?.add(requestInterface!!.onLogin("password","nas@123","a.samir")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError))
    }
    private fun handleResponse(androidList: Response) {
        println("response is "+androidList.toString())
    }
    private fun handleError(error: Throwable) {
        println("error")
        error.printStackTrace()
    }


}