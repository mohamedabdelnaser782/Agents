package com.agentsapp.naas.naasagents.viewmodels

import android.app.Activity
import android.arch.lifecycle.ViewModel
import android.content.Intent
import com.agentsapp.naas.naasagents.ui.activity.ActivityAddNotes
import com.agentsapp.naas.naasagents.ui.activity.Home_Screen

class NotesViewModel (activity: Activity): ViewModel() {
    val activity=activity

    fun goToAddNotes(){
        val intent = Intent(activity, ActivityAddNotes::class.java)
        activity.overridePendingTransition(0,0)
        activity.startActivity(intent)
        activity.overridePendingTransition(0,0)
    }
}