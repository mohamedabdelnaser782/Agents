package com.agentsapp.naas.naasagents.utl

class Constants {
    companion object{

        const val Tag_FrgmentHome = "Home"
        const val Tag_FrgmentLogin = "login"
        const val Tag_FrgmentNotes = "FragmentNotes"
        const val Tag_FrgmentProfile = "Profile"
        const val Tag_FrgmentEmployees = "Employees"

        const val Tag_InputCompanyName = "companyname"
        const val Tag_InputRespName = "RespName"
        const val Tag_InputRespPhone = "RespPhone"
        const val Tag_InputNotes = "InputNotes"
        const val Tag_InputVisitNumber = "VisitNumber"




    }
}