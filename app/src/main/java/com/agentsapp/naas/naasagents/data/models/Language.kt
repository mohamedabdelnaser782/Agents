package com.agentsapp.naas.naasagents.data.models

import android.content.Context
import com.agentsapp.naas.naasagents.data.local.SaveShared

class Language(context: Context) {

    private val key=this.javaClass.name

   private val saveShared=SaveShared(key)

    var lang:String
    get()=saveShared.getPreferences("lang")
    set(value) {
        saveShared.saveElement("lang",value)
    }

    fun isLogForUser():Boolean{
        return !lang.equals("")
    }
}