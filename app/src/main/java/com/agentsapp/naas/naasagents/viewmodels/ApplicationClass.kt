package com.agentsapp.naas.naasagents.viewmodels

import android.app.Application
import android.content.Context
import android.os.Handler
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import android.widget.Toast

open class ApplicationClass : MultiDexApplication() {

    companion object {
        @JvmField
        var context: Context? = null
        // Not really needed since we can access the variable directly.
        @JvmStatic fun getMyApplicationContext(): Context? {
            return context
        }
    }





    fun toast_MsgApp(any: Any){
        Handler().post {
            Toast.makeText(applicationContext,": $any",Toast.LENGTH_LONG);
        }
    }
    fun toast_MsgTest(any: Any){
        Handler().post {
            Toast.makeText(applicationContext,": $any",Toast.LENGTH_LONG);
        }
    }
    override fun onCreate() {
        super.onCreate()
        ApplicationClass.context = applicationContext

        MultiDex.install(applicationContext)

    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

}