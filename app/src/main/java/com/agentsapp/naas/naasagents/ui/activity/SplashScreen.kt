package com.agentsapp.naas.naasagents.ui.activity

import android.content.Context
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import com.agentsapp.naas.naasagents.R
import com.agentsapp.naas.naasagents.data.models.Language
import com.agentsapp.naas.naasagents.utl.ViewUtl
import com.agentsapp.naas.naasagents.ui.fragment.FragmentLogin
import com.agentsapp.naas.naasagents.utl.LanguageUtilty
import com.agentsapp.naas.naasagents.viewmodels.SplashViewModel
import java.util.*

class SplashScreen : FragmentActivity(),FragmentLogin.OnFragmentInteractionListener {
    val  viewUtl:ViewUtl=ViewUtl()
    var viewModel:SplashViewModel?=null
    override fun onFragmentInteraction(uri: Uri) {

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        viewModel= SplashViewModel(this)

        try {
            viewModel!!.inflateLogin()
        }catch (e:Exception){
            e.printStackTrace()
        }


    }


    fun move(container: Int, fragment: Fragment, tag: String, manager: FragmentManager) {
        val fragmentTransaction: FragmentTransaction= manager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.animator.slid_up,R.animator.slid_down)
        fragmentTransaction.replace(container, fragment, tag)
        fragmentTransaction.addToBackStack(null).commit()
    }

    override fun onBackPressed() {
        finish()
    }

     override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext((LanguageUtilty(newBase).wrap(newBase,Language(newBase).lang)))
         println("Language(newBase).lang) "+Language(newBase).lang);
    }


}
