package com.agentsapp.naas.naasagents.viewmodels

import android.app.Activity
import android.arch.lifecycle.ViewModel
import com.agentsapp.naas.naasagents.data.models.Note
import com.agentsapp.naas.naasagents.ui.dialog.CustomeProgressDialog
import com.agentsapp.naas.naasagents.utl.Constants

class AddNotesViewModel(activity: Activity):ViewModel() {
    val activity=activity
    val customeProgressDialog= CustomeProgressDialog(activity)
    var note: Note? =null

    init {
        note= Note()
    }
    fun showProgress(){
        customeProgressDialog.show()
    }

    fun closProgress(){
        try {
            if (!customeProgressDialog.isShowing){
                customeProgressDialog.dismiss()
            }
        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    fun onClickSave(){
        println("click save button and save result will ${note!!.validateData()}")

        if (note!!.validateData()) {
            showProgress()
        }else{
            println("validate data")
        }
    }

    fun afterTextChange(tag:String,text:String){
        println(""+tag)
        when (tag) {
            Constants.Tag_InputCompanyName -> note!!.comNem=text
            Constants.Tag_InputNotes ->   note!!.comNotes=text
            Constants.Tag_InputRespName -> note!!.respName=text
            Constants.Tag_InputRespPhone -> note!!.respPhone=text
            Constants.Tag_InputVisitNumber -> note!!.visitNumber=text.toInt()
            else -> { // Note the block
                print("x is neither 1 nor 2")
            }
        }

    }
}