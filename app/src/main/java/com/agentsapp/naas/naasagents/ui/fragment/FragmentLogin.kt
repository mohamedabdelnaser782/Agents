package com.agentsapp.naas.naasagents.ui.fragment

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.agentsapp.naas.naasagents.R
import com.agentsapp.naas.naasagents.data.models.Language
import com.agentsapp.naas.naasagents.ui.activity.Home_Screen
import com.agentsapp.naas.naasagents.ui.activity.SplashScreen
import com.agentsapp.naas.naasagents.viewmodels.LoginViewModel
import com.agentsapp.naas.naasagents.utl.LanguageUtilty
import kotlinx.android.synthetic.main.fragment_login.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [FragmentLogin.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [FragmentLogin.newInstance] factory method to
 * create an instance of this fragment.
 *
 */

class FragmentLogin : Fragment() {
    var mModel: LoginViewModel? = null

    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }


    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
       val mview=inflater.inflate(R.layout.fragment_login, container, false);
        requireActivity(). getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        initViews(mview)
        mModel= LoginViewModel(requireActivity())
        LanguageUtilty(requireActivity()).getCurrentLang()
        LoginViewModel(requireActivity()).userLiveData.observe(this, Observer {
            user->
            println("user is "+user.toString())
        })
     /*   mModel = ViewModelProviders.of(this).get(LoginViewModel::class.java!!)

        val nameObserver = Observer<User> { user ->
            println("name has been changed ${user.toString()}")
        }

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        mModel!!.getCurrentName("f").observe(this, nameObserver)*/



        return mview
    }
    fun initViews(any: View){
       any.btnLogin.setOnClickListener {view->
            mModel!!.onLoginClicked()
        }
       any.inputUsername.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                println("input user name "+editable.toString())
                mModel!!.afterUserNameTextChanged(editable.toString())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
       any. inputPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                println("input password is : "+editable.toString())
                mModel!!.afterPasswordTextChanged(editable.toString())
            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        any.btnArabic.setOnClickListener { view-> mModel!!.onClickChangeLanguage("ar")}
        any.btnEnglish.setOnClickListener { view-> mModel!!.onClickChangeLanguage("en")}
    }
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }
    override fun onDetach() {
        super.onDetach()
        listener = null
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }
    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentLogin.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                FragmentLogin().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onAttachFragment(childFragment: Fragment?) {
        super.onAttachFragment(childFragment)
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        println("chang confid in fragment")
    }

}


