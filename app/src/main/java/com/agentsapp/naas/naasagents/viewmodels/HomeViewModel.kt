package com.agentsapp.naas.naasagents.viewmodels

import android.app.Activity
import android.arch.lifecycle.ViewModel
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import com.agentsapp.naas.naasagents.R
import com.agentsapp.naas.naasagents.data.models.Language
import com.agentsapp.naas.naasagents.ui.dialog.LanguageDialog
import com.agentsapp.naas.naasagents.ui.dialog.LogoutDialog
import com.agentsapp.naas.naasagents.ui.fragment.FragmentEmployees
import com.agentsapp.naas.naasagents.ui.fragment.FragmentHomeMenu
import com.agentsapp.naas.naasagents.ui.fragment.FragmentNotes
import com.agentsapp.naas.naasagents.ui.fragment.FragmentProfile
import com.agentsapp.naas.naasagents.utl.Constants
import com.agentsapp.naas.naasagents.utl.LanguageUtilty
import com.agentsapp.naas.naasagents.utl.ViewUtl
import io.reactivex.Observable

class HomeViewModel(activity: FragmentActivity) : ViewModel() {
    val  viewUtl:ViewUtl=ViewUtl()
    val activity=activity
    var isRefresh:Boolean=false

    fun moveToNotesFragment(){
        val fragmentNotes:Fragment=FragmentNotes()
         viewUtl. move(R.id.homeContainer, fragmentNotes, Constants.Tag_FrgmentNotes, activity.supportFragmentManager)
    }

    fun moveToEmployeesFragment(){
        val fragmentEmployees:Fragment=FragmentEmployees()
        viewUtl. move(R.id.homeContainer, fragmentEmployees, Constants.Tag_FrgmentEmployees, activity.supportFragmentManager)
    }


    fun inflateHomeContent(){

        println("isrefresh${isRefresh}")
        if (!isRefresh){
        val fragmentHomeMenu:Fragment=FragmentHomeMenu()
        viewUtl. move(R.id.homeContainer, fragmentHomeMenu, Constants.Tag_FrgmentHome, activity.supportFragmentManager,false)
        }else{
            isRefresh=false
            println("isrefresh remove ${isRefresh}")
        }
    }

    fun backToHome(extWhenFound:Boolean=false){
        val now = activity.supportFragmentManager.findFragmentByTag(Constants.Tag_FrgmentHome)
        println("now ${now.isVisible}")
        if (now != null && now.isVisible) {
            if (extWhenFound)
            activity.moveTaskToBack(true)
        } else {
            activity.supportFragmentManager.popBackStack()
        }
    }

    fun actionLogout(){
        val dialogLogoutDialog=LogoutDialog(activity)
        dialogLogoutDialog.show()
    }

    fun actionLanguage(){
        val dialogLanguageDialog=LanguageDialog(activity)
        dialogLanguageDialog.show()
    }

    fun moveToProfile(){
        val now=activity.supportFragmentManager.findFragmentByTag(Constants.Tag_FrgmentProfile)
        if (now!=null&&now.isVisible) {
            println("now ${now.isVisible}")
        }else{
            val fragmentProfile: Fragment = FragmentProfile()
            viewUtl.move(R.id.homeContainer, fragmentProfile, Constants.Tag_FrgmentProfile,activity.supportFragmentManager)
        }
    }

    fun changeLanguage(language:String){
      val fragment= getCurrentFragment()
        if (LanguageUtilty(activity).isChangeLanguage(language)){
            Language(activity).lang=language
            ViewUtl().refresh(activity,Intent(activity,activity::class.java).putExtra("isRefresh",true))
            onChangeLanguage(fragment)
        }
    }

    fun onChangeLanguage(fragment: Fragment){
        println("tag is :${fragment.tag}")
        ViewUtl().move(R.id.homeContainer,fragment,fragment.tag.toString(),activity.supportFragmentManager,false)
        hasRefresh()

    }

    fun getCurrentFragment():Fragment{
        var fragment:Fragment?
        if (getFragment(Constants.Tag_FrgmentProfile).isVisible){
            fragment=getFragment(Constants.Tag_FrgmentProfile)
        }else if (getFragment(Constants.Tag_FrgmentHome).isVisible){
            fragment=getFragment(Constants.Tag_FrgmentHome)
        }else if (getFragment(Constants.Tag_FrgmentEmployees).isVisible){
            fragment=getFragment(Constants.Tag_FrgmentEmployees)
        }else{
            fragment=getFragment(Constants.Tag_FrgmentNotes)
        }
        println("fra${fragment.tag}")
        return fragment
    }

    fun getFragment(tag:String):Fragment{
         try {
            val now=activity.supportFragmentManager.findFragmentByTag(tag)
             if ( now!= null&&now.isVisible){
                 return  now
             }else{
                 return Fragment()
             }
        }catch (e:Exception) {
            e.printStackTrace()
            return  Fragment()
        }
    }

  fun hasRefresh(){
     return try {
      isRefresh= activity.intent.extras.getBoolean("isRefresh")
     } catch (e:Exception){
         e.printStackTrace()
     }
  }
}