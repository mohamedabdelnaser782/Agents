package com.agentsapp.naas.naasagents.data.webservice

import com.agentsapp.naas.naasagents.utl.ApiUtl
import io.reactivex.Observable
import retrofit2.http.*
import retrofit2.http.POST




interface ApiSevice {

    @FormUrlEncoded
    @POST(ApiUtl.LoginUrl)
    fun onLogin(@Field("grant_Type") grant_Type: String,@Field("UserName") userName: String,@Field("Password") password: String): Observable<Response>


}