package com.agentsapp.naas.naasagents.data.webservice

import com.google.gson.annotations.SerializedName

class Response {

    @field:SerializedName("access_token")
    var accessToken: String? = ""


    @field:SerializedName("token_type")
    var tokenType: String? = ""


    @field:SerializedName("expires_in")
    var expiresIn: Int? = 0


    @field:SerializedName("error")
    var error: String? = ""

    override fun toString(): String {
        return "Response(accessToken=$accessToken, tokenType=$tokenType, expiresIn=$expiresIn, error=$error)"
    }


}