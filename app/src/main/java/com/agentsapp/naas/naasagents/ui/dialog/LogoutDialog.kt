package com.agentsapp.naas.naasagents.ui.dialog

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import com.agentsapp.naas.naasagents.R

class LogoutDialog(activity: Activity) : Dialog(activity) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_check_logout)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }
}