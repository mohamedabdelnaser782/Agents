package com.agentsapp.naas.naasagents.ui.dialog

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.view.Window
import com.agentsapp.naas.naasagents.R
import com.agentsapp.naas.naasagents.utl.LanguageUtilty
import com.agentsapp.naas.naasagents.viewmodels.HomeViewModel
import kotlinx.android.synthetic.main.dialog_language.*

class LanguageDialog(activity: FragmentActivity) : Dialog(activity) {

    val activity=activity
    var homeViewModel:HomeViewModel?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_language)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        homeViewModel= HomeViewModel(activity)


        if (LanguageUtilty(activity).getCurrentLang().equals("en")){
            radioButton_en.isChecked=true
        }else{
            radioButton_ara.isChecked=true
        }


        group_lang.setOnCheckedChangeListener { group, checkedId ->

            when(checkedId){
                R.id.radioButton_ara->homeViewModel!!.changeLanguage("ar")
                R.id.radioButton_en->homeViewModel!!.changeLanguage("en")
            }
        }
    }
}