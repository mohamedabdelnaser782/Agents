package com.agentsapp.naas.naasagents.viewmodels

import android.app.Activity
import android.arch.lifecycle.ViewModel
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.FragmentActivity
import com.agentsapp.naas.naasagents.R
import com.agentsapp.naas.naasagents.ui.fragment.FragmentLogin
import com.agentsapp.naas.naasagents.utl.ViewUtl
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit


class SplashViewModel(activity: FragmentActivity) : ViewModel() {

    val activity=activity
    fun inflateLogin(){
        println("is lunch"+isLauncher())
        if (!isLauncher()) {
            Observable.timer(3000, TimeUnit.MILLISECONDS)
                    .map { o -> moveToFragment() }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe()
            Handler().postDelayed({
                /* Create an Intent that will start the Menu-Activity. */

            }, 3000)
        }else{
            moveToFragment(false)
        }

    }

    private fun moveToFragment(isLauncher: Boolean=true){
        val viewUtl =ViewUtl()
        var employeeList: FragmentLogin? = FragmentLogin()
        if (employeeList != null) {
            viewUtl. move(R.id.container, employeeList, "EmployeeList", activity.supportFragmentManager,isLauncher)
        }
    }


    fun isLauncher():Boolean{
        try {
            val bundle=activity.intent.extras
            val b=bundle!!.getBoolean("defualt")
            println("bundl$b")
            return bundle.getBoolean("defualt")
        }catch (e:Exception){
            e.printStackTrace()
            return false
        }
    }
}