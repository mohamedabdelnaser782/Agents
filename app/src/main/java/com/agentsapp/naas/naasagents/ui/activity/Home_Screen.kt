package com.agentsapp.naas.naasagents.ui.activity

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.agentsapp.naas.naasagents.R
import com.agentsapp.naas.naasagents.viewmodels.LoginViewModel
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.net.Uri
import android.support.annotation.Nullable
import android.support.v4.app.Fragment
import android.view.Menu
import com.agentsapp.naas.naasagents.data.models.User
import com.agentsapp.naas.naasagents.ui.fragment.FragmentEmployees
import com.agentsapp.naas.naasagents.ui.fragment.FragmentHomeMenu
import com.agentsapp.naas.naasagents.ui.fragment.FragmentNotes
import com.agentsapp.naas.naasagents.viewmodels.HomeViewModel
import kotlinx.android.synthetic.main.content_activity_home.*
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import com.agentsapp.naas.naasagents.data.models.Language
import com.agentsapp.naas.naasagents.ui.fragment.FragmentProfile
import com.agentsapp.naas.naasagents.utl.Constants
import com.agentsapp.naas.naasagents.utl.LanguageUtilty


class Home_Screen : AppCompatActivity() ,FragmentNotes.OnFragmentInteractionListener,FragmentEmployees.OnFragmentInteractionListener,FragmentHomeMenu.OnFragmentInteractionListener,
FragmentProfile.OnFragmentInteractionListener{

    override fun onFragmentInteraction(uri: Uri) {
        println("onFragmentInteraction :$uri")
    }

    var mModel:HomeViewModel?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_screen)
        mModel=HomeViewModel(this)
        initViews()
        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.


    }

   fun initViews() {
       mModel!!.inflateHomeContent()
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.actionLanguageSetting -> {
            println("change language popup")
            mModel!!.actionLanguage()
            true
        }
        R.id.actionProfile -> {
            mModel!!.moveToProfile()
            true
        }
        R.id.action_home -> {
            mModel!!.inflateHomeContent()
            true
        }
        R.id.actionLogout -> {
            mModel!!.actionLogout()
            true
        }else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
          mModel!!.backToHome(true)
    }
    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext((LanguageUtilty(newBase).wrap(newBase, Language(newBase).lang)))
        println("Language(newBase).lang) "+ Language(newBase).lang);
    }

}
