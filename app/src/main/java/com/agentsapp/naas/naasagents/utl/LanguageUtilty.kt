package com.agentsapp.naas.naasagents.utl

import com.agentsapp.naas.naasagents.data.models.Language
import java.util.*
import android.os.Build
import android.annotation.TargetApi
import android.content.Context
import android.content.ContextWrapper
import android.content.res.Configuration


class LanguageUtilty (base: Context): ContextWrapper(base) {

    val activity=base

    fun getCurrentLang():String{
        val language=Language(activity)
        if (language.isLogForUser()){
            println("local is ${language.lang}")
            return language.lang
        }else {
            val locale = Locale.getDefault().getLanguage()
            println("local is $locale")
            return locale
        }
    }

    fun isChangeLanguage(language: String):Boolean{

            return !getCurrentLang().equals(language)

    }
    fun wrap(context: Context, language: String): ContextWrapper {
        var context = context
        val config = context.getResources().getConfiguration()
        var sysLocale: Locale? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            sysLocale = getSystemLocale(config)
        } else {
            sysLocale = getSystemLocaleLegacy(config)
        }

        if (language != "") {
            val locale = Locale(language)
            Locale.setDefault(locale)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                setSystemLocale(config, locale)
            } else {
                setSystemLocaleLegacy(config, locale)
            }
            context = context.createConfigurationContext(config)
        }
        return LanguageUtilty(context)
    }

    fun getSystemLocaleLegacy(config: Configuration): Locale {
        return config.locale
    }

    @TargetApi(Build.VERSION_CODES.N)
    fun getSystemLocale(config: Configuration): Locale {
        return config.getLocales().get(0)
    }

    fun setSystemLocaleLegacy(config: Configuration, locale: Locale) {
        config.locale = locale
    }

    @TargetApi(Build.VERSION_CODES.N)
    fun setSystemLocale(config: Configuration, locale: Locale) {
        config.setLocale(locale)
    }
}