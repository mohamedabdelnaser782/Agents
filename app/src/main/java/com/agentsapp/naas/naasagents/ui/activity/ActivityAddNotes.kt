package com.agentsapp.naas.naasagents.ui.activity

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.agentsapp.naas.naasagents.R
import com.agentsapp.naas.naasagents.ui.dialog.CustomeProgressDialog
import com.agentsapp.naas.naasagents.utl.Constants
import com.agentsapp.naas.naasagents.viewmodels.AddNotesViewModel
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks
import com.github.ksoichiro.android.observablescrollview.ScrollState
import kotlinx.android.synthetic.main.activity_add_notes.*

class ActivityAddNotes: AppCompatActivity() {

    var addNotesViewModel: AddNotesViewModel? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_notes)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        addNotesViewModel= AddNotesViewModel(this)
        initViews()

    }

    fun initViews(){
        btnSave.setOnClickListener { view-> addNotesViewModel!!.onClickSave() }

        inputComName.addTextChangedListener(object :TextWatcher{
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                println("text change "+s.toString())
                addNotesViewModel!!.afterTextChange(Constants.Tag_InputCompanyName,s.toString())
            }

        })

        inputNotes.addTextChangedListener(object :TextWatcher{
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                addNotesViewModel!!.afterTextChange(Constants.Tag_InputNotes,s.toString())
            }

        })

        inputRespName.addTextChangedListener(object :TextWatcher{
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                addNotesViewModel!!.afterTextChange(Constants.Tag_InputRespName,s.toString())
            }

        })

        inputRespPhone.addTextChangedListener(object :TextWatcher{
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                addNotesViewModel!!.afterTextChange(Constants.Tag_InputRespPhone,s.toString())
            }

        })

        inputVisitNum.addTextChangedListener(object :TextWatcher{
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun afterTextChanged(s: Editable?) {
                addNotesViewModel!!.afterTextChange(Constants.Tag_InputVisitNumber,s.toString())
            }

        })

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }


}
