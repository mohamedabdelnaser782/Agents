package com.agentsapp.naas.naasagents.data.webservice

import com.agentsapp.naas.naasagents.data.models.User
import com.agentsapp.naas.naasagents.utl.ApiUtl
import com.google.gson.GsonBuilder
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.security.cert.CertificateException
import java.util.concurrent.TimeUnit
import javax.net.ssl.*

open class ApiClient {


    var retrofit: Retrofit? = null

    var RETROFIT_CLIENT: ApiSevice? = null


    fun getInstance(): ApiSevice? {
        //if REST_CLIENT is null then set-up again.
        if (RETROFIT_CLIENT == null) {
            setupRestClient()
        }
        return RETROFIT_CLIENT
    }

    private fun setupRestClient() {

        val retrofit = Retrofit.Builder()
                .baseUrl(ApiUtl.ServerPath)
                .client(ApiClient().getOkHttp())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        RETROFIT_CLIENT = retrofit.create(ApiSevice::class.java)
    }


    fun getOkHttp():OkHttpClient{
        val okHttpClient = OkHttpClient.Builder()
                .readTimeout(5, TimeUnit.SECONDS)
                .connectTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .addInterceptor ( BasicAuthInterceptor()).build();
        return okHttpClient
    }
    fun getClient(): Retrofit {

        var token = User().accessToken
        val okHttpClient = OkHttpClient.Builder()
                .readTimeout(5, TimeUnit.SECONDS)
                .connectTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .addInterceptor ( BasicAuthInterceptor()).build();

        if (retrofit == null) {
            /*  retrofit = new Retrofit.Builder()
                    .baseUrl(Constants_Event.Api_PATH)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().serializeNulls().create()))
                    .build();*/
            retrofit = Retrofit.Builder()
                    .baseUrl(ApiUtl.ServerPath)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder().serializeNulls().create()))
                    .build()
        }
        return this!!.retrofit!!
    }

    class BasicAuthInterceptor(user: String, password: String) : Interceptor {

        private val credentials: String

        init {
            this.credentials = Credentials.basic(user, password)
        }

        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
            val request = chain.request()
            val authenticatedRequest = request.newBuilder()
                    .header("Authorization", credentials).build()
            return chain.proceed(authenticatedRequest)
        }

    }

    fun getUnsafeOkHttpClient(): OkHttpClient.Builder {
        try {
            // Create a trust manager that does not validate certificate chains
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                }

                override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                    return arrayOf()
                }
            })

            // Install the all-trusting trust manager
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())

            // Create an ssl socket factory with our all-trusting manager
            val sslSocketFactory = sslContext.socketFactory

            val builder = OkHttpClient.Builder()
            builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            builder.hostnameVerifier { hostname, session -> true }
            return builder
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }
}

class BasicAuthInterceptor:Interceptor{
    override fun intercept(chain: Interceptor.Chain?): Response? {
        val credentials = Credentials.basic("ufelix","ufelix");
        val request:Request= chain!!.request()
        val authenticatedRequest:Request=request.newBuilder().header("Authorization",credentials)
                .addHeader("Content-type", "application/x-www-form-urlencoded").build()
        return chain.proceed(authenticatedRequest)
    }
}

