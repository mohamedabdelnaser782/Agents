package com.agentsapp.naas.naasagents.data.models

import com.google.gson.annotations.SerializedName

class Note (comName:String="",respName:String="",respPhone:String="",comNotes:String="",visitNumber:Int=0){

    @SerializedName("")  var comNem=comName
    @SerializedName("")  var respName=respName
    @SerializedName("")  var respPhone=respPhone
    @SerializedName("")  var comNotes=comNotes
    @SerializedName("")  var visitNumber=visitNumber
    fun validateData():Boolean{
        println("note is ${this.toString()}")

        return respName.length>3&&respPhone.length>11&&comNotes.length>0&&visitNumber>0
    }

    override fun toString(): String {
        return "Note(comNem='$comNem', respName='$respName', respPhone='$respPhone', comNotes='$comNotes', visitNumber='$visitNumber')"
    }


}